# SQL Injection Lab
Try to get the full control of the web server!

## Run server
Run server with simple command `docker-compose up`.

### Server API:
 - `GET /api/map` - Gets all key-value pairs of the map
 - `GET /api/map/:key` - Get value of the specific key
 - `PUT /api/map` - Add or update value by key. Body for put `{"key": "some-key", "value": "some-value"}` 
 - `DELETE /api/map/:key` - Delete a value from the map